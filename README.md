## Pipeline
[![pipeline status](https://gitlab.com/hanifariana/story-8/badges/master/pipeline.svg)](https://gitlab.com/hanifariana/story-8/-/commits/master)

heroku: https://story8-hanifa.herokuapp.com/

## Coverage
[![Coverage](https://gitlab.com/hanifariana/story-8/badges/master/coverage.svg)](https://gitlab.com/hanifariana/story-8/-/commits/master)
